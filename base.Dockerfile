FROM ubuntu:focal
ENV DEBIAN_FRONTEND=noninteractive
# Has to be changed for newer release
ENV PATH="/usr/local/texlive/2024/bin/x86_64-linux:${PATH}"
