FROM registry.git.rwth-aachen.de/latex-templates1/latex-docker/base:latest

RUN apt-get update -q \
    && apt-get install -qy build-essential wget libfontconfig1 p7zip-full \
    && rm -rf /var/lib/apt/lists/*

# Install TexLive with scheme-full
RUN wget --progress=dot:giga http://mirror.ctan.org/systems/texlive/Images/texlive.iso \
    && 7z x texlive.iso -o/install-tl-unx \
    && echo "selected_scheme scheme-full" >> /install-tl-unx/texlive.profile \
    && chmod +x /install-tl-unx/install-tl \
    && /install-tl-unx/install-tl -profile /install-tl-unx/texlive.profile \
    && rm -r /install-tl-unx \
    && rm texlive.iso
